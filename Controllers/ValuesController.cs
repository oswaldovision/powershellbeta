﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using powershellbeta.Models;

namespace powershellbeta.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }

        // POST api/values
        [Route("powershellScript")]
        [HttpPost]
        public HttpResponseMessage PostScript([FromBody]scriptModel model)
        {
            var exe = new execPs();

            var result = exe.RunScript(model.scriptPs);

            return new HttpResponseMessage()
            {
                Content = new StringContent(result)
            };
        }

    }
}
